$("body").prepend(`
    <div id="bz-admin-icon">
      <div class="link-editor-shaft">
        <div class="inputcontainer">
          <label class="linklabel linkhref-label">link href</label>
          <input type="text" class="linkhref">
          <div class="bzbuttons">
            <div class="bzbutton bzlink bzcancel" onclick="bzadminstagelink($('#bz-admin-icon .link-editor-shaft .inputcontainer').attr('target'))"> reset </div>
            <div class="bzbutton bzlink bzaccept"  onclick="bzadminsetlink()" > save </div>
          </div>
        </div>
      </div>
      <div class="image-editor-shaft">
        <div class="image-editor-container">
        </div>
      </div>
      <div class="main-menu-shaft">
        <div class="menu">
          <ul class="bzlist">
          <li class="bzlistitm edittext" onclick="bzadminedittext(!this.classList.contains('active'))">Edit Text</li>
          <li class="bzlistitm editlink" onclick="bzadimineditlink(!this.classList.contains('active'))">Edit Link</li>
          <li class="bzlistitm editpicture" onclick="window.bzadmin.imgedit.enable(!this.classList.contains('active'))">Edit Picture</li>
          <li class="bzlistitm" > </li>
          <li class="bzlistitm" onclick="bzadminregisterpagecontent()">Save Changes</li>
          <li class="bzlistitm " onclick="location.reload(true);">Reset</li>
          <li class="bzlistitm" onclick="location.href='/admin';" >Admin page</li>
          <li class="bzlistitm" onclick="bzadminlogout()">LogOut</li>
          </ul>
        </div>
      </div>
    </div>
  `);

$("#bz-admin-icon>*")
  .on("mouseup", function(e) {
    e = e || window.event;
    e.stopPropagation();
    event.stopPropagation();
  })
  .on("mousedown", function(e) {
    e = e || window.event;
    e.stopPropagation();
    event.stopPropagation();
  });

bzadmindrag($("#bz-admin-icon")[0], function() {
  event.target.classList.toggle("visible");
});

// draggable buttons
function dragElement(elmnt) {
  // e = e || window.event;
  // e.preventDefault();
  // e.stopPropagation();
  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  elmnt.getElementsByClassName("dragable")[0].onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = elmnt.offsetTop - pos2 + "px";
    elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
  }

  function closeDragElement(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function bzadmindrag(elmnt, onclickfunction) {
  // e = e || window.event;
  // e.preventDefault();
  // e.stopPropagation();
  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0,
    ty = 0,
    tx = 0,
    wh = window.innerHeight - elmnt.clientHeight,
    ww = window.innerWidth - elmnt.clientWidth;

  elmnt.onmousedown = dragMouseDown;

  var dragtimer;
  //   var dragtimer = setTimeout(function() {
  // }, 500);
  // document.onmouseup = function() {
  //   if (Date.now() - dragtimer < 300) {
  //     clearTimeout(dragtime);

  //   }
  // };
  function dragMouseDown(e) {
    dragtimer = Date.now();
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    // calculate the new cursor position:
    ty = elmnt.offsetTop - pos4 + e.clientY;
    tx = elmnt.offsetLeft - pos3 + e.clientX;
    pos3 = e.clientX;
    pos4 = e.clientY;

    if (ty <= 0 || tx <= 0 || tx > ww || ty > wh) return;
    elmnt.style.top = ty + "px";
    elmnt.style.left = tx + "px";
  }

  function closeDragElement(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
    if (Date.now() - dragtimer < 150) {
      onclickfunction();
    }
  }
}

// register changes
window.bzadminregisterpagecontent = function() {
  var data = {
    pageurl: window.location.pathname.slice(1),
    pagecontent: {},
    template_id: window.template_id,
  };
  $("[data-bz-id]").each(function(i, el) {
    data.pagecontent["data_id_" + $(el).data("bz-id")] = $(el)
      .clone() //clone the element
      .children() //select all the children
      .remove() //remove all the children
      .end() //again go back to selected element
      .text();
  });
  $("[data-bz-href-id]").each(function(i, el) {
    data.pagecontent["data_id_" + $(el).data("bz-href-id")] = $(el).attr(
      "href"
    );
  });
  // console.log(data);
  // return;
  $.ajax({
    url: "/admin/contentedit",
    type: "POST",
    data: data,
    success: function(data) {
      // console.log(data);
      alert("تغییرات با موفقیت ثبت گردید");
    },
    error: function(data) {
      alert("خطا ...");
    },
  });
};

//logout
window.bzadminlogout = function() {
  $.ajax({
    url: "/logout",
    type: "POST",
    success: function(data) {
      // console.log(data);
      location.reload(true);
    },
    error: function(data) {
      alert("خطا ...");
    },
  });
};

//editing text
window.bzadminedittext = function(en) {
  if (en) {
    bzadimineditlink(false);
    window.bzadmin.imgedit.enable(false);
    $("#bz-admin-icon .main-menu-shaft .edittext").addClass("active");
    $("[data-bz-id]")
      .addClass("adminedit")
      .attr("contenteditable", "plaintext-only")
      .keypress(function(e) {
        return e.which != 13;
      });
  } else {
    $("#bz-admin-icon .main-menu-shaft .edittext").removeClass("active");
    $("[data-bz-id]")
      .removeClass("adminedit")
      .attr("contenteditable", false);
  }
};

//editing links
window.bzadimineditlink = function(en) {
  if (en) {
    bzadminedittext(false);
    window.bzadmin.imgedit.enable(false);
    $("a").each(function(i, el) {
      var el = $(el);
      var os = el.offset();
      $("body").prepend(`
    <div class="bzadmin-link-edit-container"
      style="
        top:${os.top}px;
        left:${os.left}px;
        width:${el.outerWidth()}px;
        height:${el.outerHeight()}px;
        font-size:calc(${el.css("font-size")} - 2px);
        "
        data-bz-linkcontainer-id="${$(el).data("bz-href-id")}"
        onclick="bzadminstagelink(${$(el).data("bz-href-id")})"
      >
      ${el.text()}
    </div>
  `);
    });
    $("#bz-admin-icon .main-menu-shaft .editlink").addClass("active");
    $(
      "#bz-admin-icon .link-editor-shaft , .bzadmin-link-edit-container"
    ).addClass("visible");

    $;
  } else {
    $(".bzadmin-link-edit-container").remove();
    $("#bz-admin-icon .main-menu-shaft .editlink").removeClass("active");
    $(
      "#bz-admin-icon .link-editor-shaft ,.bzadmin-link-edit-container"
    ).removeClass("visible");
  }
};

window.bzadminstagelink = function(lid) {
  $(".bzadmin-link-edit-container").removeClass("active");
  $(`[data-bz-linkcontainer-id=${lid}]`).addClass("active");
  $("#bz-admin-icon .link-editor-shaft .linkhref").val(
    $(`[data-bz-href-id=${lid}]`).attr("href")
  );

  $("#bz-admin-icon .link-editor-shaft .inputcontainer").attr("target", lid);
};

window.bzadminsetlink = function() {
  lid = $("#bz-admin-icon .link-editor-shaft .inputcontainer").attr("target");
  $(`[data-bz-href-id=${lid}]`).attr(
    "href",
    $("#bz-admin-icon .link-editor-shaft .linkhref").val()
  );
};

//edditing images

window.bzadmin = {
  imgedit: {
    vars: {
      uploading: false,
    },
    enable: function(en) {
      if (en) {
        bzadminedittext(false);
        bzadimineditlink(false);
        $("#bz-admin-icon .main-menu-shaft .editpicture").addClass("active");
        $("#bz-admin-icon .image-editor-shaft ").addClass("visible");
        window.bzadmin.imgedit.changingwinonclick();
      } else {
        $("#bz-admin-icon .main-menu-shaft .editpicture").removeClass("active");
        $("#bz-admin-icon .image-editor-shaft ").removeClass("visible");
        $("#bz-admin-icon .image-editor-shaft .bzcancel").trigger("click");
        $(".bzadminpictureactive")
          .removeClass("bzadminpictureactive")
          .removeAttr("bzimageid");
        window.onclick = window.bzwindowonclickcontainer;
      }
    },
    changingwinonclick: function() {
      window.bzwindowonclickcontainer = window.onclick;
      var i = 1;
      window.onclick = function() {
        let target = $(event.target),
          background,
          beforebackground,
          afterbackground,
          // i,
          images = [];
        if (target.closest("#bz-admin-icon").length) return;
        event.preventDefault();
        event.stopPropagation();
        $(".bzadminpictureactive").removeClass("bzadminpictureactive");
        console.log("test", target.find("*"));
        target.find("*").each(function(i, e) {
          parsedimgs = window.bzadmin.imgedit.extractimages($(e), i);
          images = images.concat(parsedimgs.images);
          i = parsedimgs.i;
        });
        do {
          parsedimgs = window.bzadmin.imgedit.extractimages(target, i);
          images = images.concat(parsedimgs.images);
          i = parsedimgs.i;
          target = target.parent();
        } while (target.parent().length);

        window.bzadmin.imgedit.stage(images);
      };
    },
    extractimages: function(target, i) {
      let images = [];
      if (target.prop("tagName") == "IMG") {
        if (target.prop("src").indexOf("data:image") == -1) {
          target.addClass("bzadminpictureactive");
          target.attr("bzimageid", i);
          images.push({ id: i, url: target.prop("src"), type: "src" });
          i++;
        }
      }

      background = window
        .getComputedStyle(target[0])
        .getPropertyValue("background-image", "");
      if (background.indexOf("data:image") == -1 && background != "none") {
        background = background.replace(/.*url\("(.+?)"\)/, "$1");
        console.log(background.slice(0, 4));
        if (background.slice(0, 4) == "http") {
          target.addClass("bzadminpictureactive");
          images.push({ id: i, url: background, type: "bg" });
          target.attr("bzimageid", i);
          i++;
        }
      }

      beforebackground = window
        .getComputedStyle(target[0], ":before")
        .getPropertyValue("background-image");

      if (
        beforebackground.indexOf("data:image") == -1 &&
        beforebackground != "none"
      ) {
        beforebackground = beforebackground.replace(/.*url\("(.+?)"\)/, "$1");
        if (background.slice(0, 4) == "http") {
          target.addClass("bzadminpictureactive");
          images.push({
            id: i,
            url: beforebackground,
            type: "bfbg",
          });
          i++;
        }
      }
      afterbackground = window
        .getComputedStyle(target[0], ":after")
        .getPropertyValue("background-image");
      if (
        afterbackground.indexOf("data:image") == -1 &&
        afterbackground != "none"
      ) {
        afterbackground = afterbackground.replace(/.*url\("(.+?)"\)/, "$1");
        if (background.slice(0, 4) == "http") {
          target.addClass("bzadminpictureactive");
          images.push({
            id: i,
            url: afterbackground,
            type: "afbg",
          });
          i++;
        }
      }
      return { images: images, i: i };
    },
    stage: function(images) {
      let stagecontainer = $(
        "#bz-admin-icon .image-editor-shaft .image-editor-container"
      );
      let resseted = new Promise(function(resolve, reject) {
        let imagecontainer = stagecontainer.children();
        if (imagecontainer.length) {
          imagecontainer.each(function(i, ele) {
            let parentdiv = $(ele),
              imagetype = parentdiv.attr("bz-image-type"),
              imageid = parentdiv.attr("bz-image-id"),
              orgimg = parentdiv.attr("bz-image-org");
            target = $(`[bzimageid=${imageid}]`);
            if (imagetype == "bg") {
              target.css("background-image", `url(${orgimg})`);
            } else if (imagetype == "src") {
              target.prop("src", orgimg);
            }
            parentdiv.find("img").prop("src", orgimg);
            let inputele = parentdiv.find("input")[0];
            // resetting file input
            inputele.value = "";
            inputele.type = "";
            inputele.type = "file";
            if (i == imagecontainer.length - 1) {
              resolve();
            }
          });
        } else {
          resolve();
        }
      });

      resseted.then(function() {
        $(
          "#bz-admin-icon .image-editor-shaft .image-editor-container>*"
        ).remove();
        images.forEach(function(i) {
          stagecontainer.prepend(`
          <div class="image-edit-row" bz-image-id="${
            i.id
          }" bz-image-org="${i.url}" bz-image-now="${i.url}" bz-image-type="${i.type}">
            <div class="bz-image-container">
              <img src="${i.url}" class="bz-imageedit">
            </div>
            <div class="bz-actions">
              <div class="bzbutton bzlink bzinfo"  onclick="$(this).siblings('input').trigger('click')" > change </div>
              <div class="bzbutton bzlink bzaccept"  onclick="bzadmin.imgedit.upload()" > upload </div>
              <div class="bzbutton bzlink bzcancel" onclick="bzadmin.imgedit.reset()"> reset </div>
              <input 
                type="file" 
                style="display:none;"
                accept=".jpeg,.bmp,.png,.jpg,.gif"
                onchange="bzadmin.imgedit.change()">
            </div>
          </div>
        `);
        });
      });
    },
    change: function() {
      ele = $(event.target);
      file = event.target.files[0];
      if (!file) return;
      if (file.size > 1500000) {
        this.alert("file is too large");
        return;
      }

      let reader = new FileReader(),
        parentdiv = ele.closest(".image-edit-row"),
        imagetype = parentdiv.attr("bz-image-type"),
        imageid = parentdiv.attr("bz-image-id");

      reader.readAsDataURL(file);
      reader.onload = function() {
        let base64img = reader.result,
          target = $(`[bzimageid=${imageid}]`);
        if (imagetype == "bg") {
          target.css("background-image", `url(${base64img})`);
        } else if (imagetype == "src") {
          target.prop("src", base64img);
        }
        parentdiv.find("img").prop("src", base64img);
      };
    },
    reset: function() {
      console.log(ele);
      let parentdiv = ele.closest(".image-edit-row"),
        imagetype = parentdiv.attr("bz-image-type"),
        imageid = parentdiv.attr("bz-image-id"),
        orgimg = parentdiv.attr("bz-image-org");
      target = $(`[bzimageid=${imageid}]`);
      if (imagetype == "bg") {
        target.css("background-image", `url(${orgimg})`);
      } else if (imagetype == "src") {
        target.prop("src", orgimg);
      }
      parentdiv.find("img").prop("src", orgimg);
      let inputele = parentdiv.find("input")[0];
      inputele.value = "";
      inputele.type = "";
      inputele.type = "file"; // resetting file input
    },
    upload: function() {
      if (window.bzadmin.imgedit.vars.uploading) {
        alert("still uploading");
        return;
      }
      ele = $(event.target);
      let file = ele.siblings("input")[0].files[0];
      let fileurl = ele.closest(".image-edit-row").attr("bz-image-org");
      fileurl = fileurl.replace(window.location.origin, "");
      if (!file) {
        alert("no file selected");
        return;
      }
      if (!window.confirm("File will be overwritten. Are you sure?")) return;
      window.bzadmin.imgedit.vars.uploading = true;
      var data = new FormData();
      data.append("file", file);
      data.append("url", fileurl);
      console.log(fileurl);
      $.ajax({
        url: "admin/contentedit/image",
        type: "POST",
        data: data,
        cache: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data) {
          console.log(data);
          ele
            .closest(".image-edit-row")
            .attr(
              "bz-image-org",
              window.location.origin + "/" + data + "?" + new Date().getTime()
            );
          window.bzadmin.imgedit.vars.uploading = false;
          alert("image uploaded");
        },
        error: function(data) {
          console.log(data);
          window.bzadmin.imgedit.vars.uploading = false;
        },
      });
    },
  },
};
