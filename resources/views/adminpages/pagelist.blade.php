@extends('layouts.adminapp')

@section('content')
<div class="container pagelist">
    <div class="row">
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="panel panel-default">
            <div class="panel-body bz-btn" data-toggle="modal" data-target="#newpageModal">
              <span class="glyphicon glyphicon-plus-sign" style="color:white;font-size:80px;"></span>
            </div>
        </div>
      </div>
      @foreach ($Pages as $p)
        <a class="col-12 col-sm-6 col-md-3 col-lg-2 pageitm" href="{{$p->url ? '/'.$p->url : '/'}}">
          <div class="panel panel-info">
              <div class="panel-heading">
                <button type="button bzboot-remove" class="close"
                    onclick="deletepage({{$p->id}});">&times;</button>
                <h6>
                    url : /{{$p->url}}
                </h6>
              </div>
              <div class="panel-body" style="min-height:150px;overflow:hidden;">
                @if($p->image)<img src="{{$p->image}}" style="width:100%;">@endif
              </div>
              <div class="panel-footer">
                
                    {{$p->title}} {{$p->description}}
              </div>
          </div>
        </a>
      @endforeach
    </div>
    <div class="col-xs-12" style="text-align: center;">
      {{ $Pages->links() }}
    </div>
</div>
<div id="newpageModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style="direction:rtl;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="float:left;">&times;</button>
        <h4 class="modal-title">ویرایش صفحه</h4>
      </div>
      <div class="modal-body modalbody" >
        <h5>تنظیمات صفحه</h5>
        <form id="page_form" method="post">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <input id="page_title" type="text" class="form-control" name="title" placeholder="عنوان صفحه">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <input id="page_url" type="text" class="form-control" name="url"  placeholder="url of page for homepage leave empty">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input id="page_description" type="text" class="form-control" name="description" placeholder="توضیحات صفحه">
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <hr class="hr">
        <h5>مدل صفحه</h5>
        <div class="btn btn-info col-xs-12 col-sm-6 col-md-4 col-lg-3 uploadbtn">
          بارگزاری مدل
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12">
          <div class="progress hidden">
            <div class="progress-bar progress-bar-striped active" role="progressbar" style="width:50%">
              0%
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row templatelist">
          @foreach ($Templates as $t)
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 templatecard" onclick="$(this).addClass('active').siblings('.active').removeClass('active');" data-tid="{{$t->id}}">
              <div class="panel panel-info">
                  <div class="panel-heading">
                    <button type="button" class="close"
                    onclick="deletetemplate({{$t->id}});">&times;</button>
                    <h6>
                      {{$t->title}}
                    </h6>
                  </div>
                  <div class="panel-body" style="min-height:150px;overflow:hidden;">
                    @if($t->image)<img src="{{$t->image}}" style="width:100%;">@endif
                  </div>
                  <div class="panel-footer">
                    {{$t->description}}
                  </div>
              </div>
            </div>
          @endforeach

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success pull-right" onclick="newpage()" >ثبت</button>

      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="filesizeerror" role="dialog" >
  <!-- data-backdrop="false" -->
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      <div class="alert alert-danger" style="margin:0;direction:rtl;">
        <strong>خطا !</strong>
        <!-- modal error text -->
        <span id="errormodaltext">
        </span>
      </div>
    </div>
  </div>
</div>
<div class="fileuploaders hidden">
  <form class="temp_form" method="post">
  <!-- {{ csrf_field() }} -->
    <input type="file" class="temp_input" accept=".zip" name="file" onchange="fileuploader(this)">
  </form>
</div>
@endsection

@push('bottom-scripts')
  <script src="/bzadminassets/js/panel.js"></script>
  <style media="screen">
    .bz-btn{
        min-height: 230px;
        overflow: hidden;
        background: #4eb7d8;
        display: flex;
        justify-content: center;
        align-items: center;
        box-shadow: -2px 2px 2px grey;
        border-radius: 7px;
    }
    .bz-btn:hover{
        transform:scale(1.01) translateY(-2px);
        box-shadow: -4px 4px 2px grey;
        transition:0.2s;
    }
    .bz-btn:active{
        transform:scale(0.99) translateY(1px);
        box-shadow: none;
        transition:0.2s;
    }
    .uploadbtn{
        margin:20px 15px;
    }
    .modalbody{
        max-height: calc(100vh - 200px);
        overflow-y: scroll;
    }
    .hr{
        width: calc(100% - 70px);
    }
    .templatecard .panel:before{
        content:" ";
        position:absolute;
        top:5px;
        left:5px;
        width:20px;
        height:20px;
        border-radius:50%;
        border:1px solid #5bc0de;
        background:white;
    }
       
    .templatecard .panel{
            position: relative;
 
        }
    .templatecard .panel-footer{
            min-height:41px;
 
        }
     .bzboot-remove.close {
            font-size: 29px;
            position: absolute;
            top: 0;
            right: 5px;
            color: red;
 
        }
    .templatecard.active .panel{
            box-shadow: 0 0 5px grey;
 
        }
    .templatecard.active .panel:before{
        background:
        url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD//gA8Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAKAP/bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIABgAGAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38rL8beNtH+Gvg/VPEPiLVNP0PQdDtZL7UdRv7hbe1sbeNS8kssjEKiKoJLMQAATXyD+zd/wAF2fgz+0x/wUN8Tfs06fp3jzw34+8P/aktpPEejjTbfWprYF5oreN3+0BhCGmUSwxlo45G4wN3xP8AGH4y69/wcuf8FD774GeB9Wk0/wDY1+Ct9DfePtZ066Zf+FhXaSHybZJYz81u8kTCEKQmyKW6LO4tY0APrb9mb9rP4lf8FfvijD4u+Gt9rXwr/ZV8I64rWniP7IIfEPxentJjujtlmQ/YtHMybZH2faJghiBhLTpEV9xeDvB+k/DzwjpXh/QdNsdF0PQ7OHT9O0+ygWC2sbaJBHFDFGoCpGiKqqqgAAADgUUAfjt/wc1/8EdviR+1P8evhD8Xv2edE1ST4napcSeDPEMukTmxkFs1vNJb38867VhjjjW6t5ZpHyyzWsQz8qH7+/4JA/8ABOXS/wDglv8AsJ+E/hbbPZXniJFbVfFWpWpLR6pq84X7RIjMqM0SBUhiLIrGKCLcA26iigD6dooooA//2Q==');
        background-size:contain;
    }

  </style>
@endpush
