<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplateGroup extends Model
{
   protected $guarded = [];
    public $timestamps = false;
}
