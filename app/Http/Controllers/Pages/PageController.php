<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PageContent;
use App\Models\Page;
use App\Models\Template;
use App\Models\Contactus;
use Cache;

class PageController extends Controller
{

  public function show(string $pageurl = null)
  {
    $page = 'App\Models\Page'::where('url' , $pageurl)->first();
    abort_if(!$page,404);

    //get last content from database
    $content = PageContent::
      where('page_id',$page->id)->
      orderBy('updated_at','desc')->
      first()->
      load('template');

    if(!$content){
        $page->delete();
        abort(404);
    }

    $pagecontent = json_decode($content->content_json, true);
    $view = view('pages.'.$content->template->view, $pagecontent);

    if(auth()->guest()) Cache::forever(url()->current(), (string) $view);

    return $view;
  }

    public function list()
    {
        // --todo -- delete page if it has no content
        return view('adminpages.pagelist',[
            'Pages' => Page::paginate(24),
            'Templates' => Template::orderBy('id','desc')->get()
        ]);

    }
  
  public function createPage(Request $request){

       $this->validate($request, [
            'title' => 'nullable|string|max:512',
            'url' => 'nullable|string|unique:pages',
            'description'=> 'sometimes|nullable|string|max:1500'
        ]);
        if($request->url == null && Page::where('url',null)->count())
            return response()->json([
                "errors" => ["url"=> ["The url has already been taken."]],
                "message"=> "The given data was invalid."
            ],422);
      $page = Page::create([
          'title'=>$request->title,
          'url'=>$request->url,
          'description'=>$request->description,
      ]);
      
      PageContent::create([
          'content_json' => Template::find($request->template_id)->default_content,
          'page_id' => $page->id,
          'template_id' => $request->template_id,
          'updated_at' => time()
      ]);
      return $page;
  }

  public function deletePage(Request $request){
    Page::find($request->id)->delete();
    PageContent::where('page_id',$request->id)->delete();
    return response()->json('success');
  }

  public function contactus(Request $request)
  {
    Contactus::create([
        'contactus_json' => json_encode($request->all(),true),
        'created_at' => time()
    ]);
        
    return response()->json(['saved'=> true]);

  }
}
