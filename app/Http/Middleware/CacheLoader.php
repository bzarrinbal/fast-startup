<?php

namespace App\Http\Middleware;

use Closure;
use Cache;

class CacheLoader
{
    /**
     * check if url is cached and returns cached data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
          ($cacheData = Cache::get(url()->current())) and
          !$request->has('nocache') and
          $request->isMethod('get') and
          auth()->guest()
        ){
          return response($cacheData);
        }
        return $next($request);
    }
}
