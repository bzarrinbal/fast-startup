function uploadtemplate() {
  alert("upload file");
}

$(function() {
  //sets global header for ajax call

  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
  });

  $(".uploadbtn").on("click", function(e) {
    $(".temp_input").trigger("click");
  });

  fileuploader = function(ele) {
    filename = ele.files[0].name;
    templatename = prompt(
      "نام مدل",
      filename.slice(0, filename.lastIndexOf("."))
    );
    if (ele.files[0].size > 30242880) {
      // 5 MB
      $("#errormodaltext").text(
        "حجم فایل بارگزاری شده نمی‌تواند از " + 30 + " مگابایت بیشتر باشد !!!"
      );
      $("#filesizeerror").modal();
      ele.value = "";
      ele.type = "";
      ele.type = "file"; // resetting file input
      return;
    }
    var pelement = $(".progress");
    var pbar = pelement.find(".progress-bar");
    pbar.css("width", 0 + "%");
    var formData = new FormData($(ele).parent()[0]);
    formData.append("template_name", templatename);
    pelement.removeClass("hidden");
    // pelement.find('.glyphicon-ok').addClass('hidden');
    // pelement.find('.glyphicon-remove').addClass('hidden');
    $.ajax({
      url: "/admin/templates/upload",
      type: "POST",
      data: formData,
      contentType: false,
      success: function(data) {
        pelement.addClass("hidden");
        // pelement.find('.glyphicon-remove').addClass('hidden');
        // pelement.find('.glyphicon-ok').removeClass('hidden');

        pbar.css("width", 0 + "%");
        pbar.text(0 + "%");
        ele.value = "";
        ele.type = "";
        ele.type = "file";
        addnewlytmeplates(data);
      },
      error: function(data) {
        // pelement.find('.glyphicon-remove').removeClass('hidden');
        pelement.addClass("hidden");
        pbar.css("width", 0 + "%");
        $("#errormodaltext").text("آپلود فایل انجام نشده !!!");
        $("#filesizeerror").modal();
        ele.value = "";
        ele.type = "";
        ele.type = "file";
      },
      xhr: function() {
        var xhr = $.ajaxSettings.xhr();
        xhr.upload.onprogress = function(e) {
          if (e.lengthComputable) {
            pbar.css("width", Math.round((e.loaded / e.total) * 100) + "%");
            pbar.text(Math.round((e.loaded / e.total) * 100) + "%");
          }
        };
        return xhr;
      },
      cache: false,
      processData: false,
    });
  };
});

function addnewlytmeplates(templates) {
  console.log(templates);

  templates.forEach(function(v, i) {
    $(
      ".templatelist"
    ).prepend(`<div class="col-12 col-sm-6 col-md-4 col-lg-3 templatecard" onclick="$(this).addClass('active').siblings('.active').removeClass('active');" data-tid="${v.id}">
      <div class="panel panel-info">
        <div class="panel-heading">
          <button type="button" class="close" onclick="deletetemplate(${
            v.id
          });">&times;</button>
          <h6>${v.title}</h6>
        </div>
        <div class="panel-body" style="min-height:150px;overflow:hidden;">
          ${v.image ? "<img src=" + v.image + ' style="width:100%;">' : ""}
                  </div>
          <div class="panel-footer">
            ${v.description}
          </div>
        </div>
      </div>`);
  });
}

function newpage() {
  var data = {
    title: $("#page_title").val(),
    url: $("#page_url").val(),
    description: $("#page_description").val(),
    template_id: $(".templatecard.active").data("tid"),
  };

  $.ajax({
    url: "/admin/pages/new",
    type: "POST",
    data: data,
    // contentType: false,
    success: function(data) {
      console.log(data);
      $("#newpageModal").modal("hide");
      $(".pagelist").append(`
        <a class="col-12 col-sm-6 col-md-3 col-lg-2" href="${
          data.url ? "/" + data.url : "/"
        }">
          <div class="panel panel-info">
              <div class="panel-heading">url: /${data.url}</div>
              <div class="panel-body" style="min-height:150px;overflow:hidden;">
               ${
                 data.image
                   ? "<img src='" + data.image + "' style='width:100%;'>"
                   : ""
               }
              </div>
              <div class="panel-footer">
                 ${data.title} ${data.description}
              </div>
          </div>
        </a>
      `);
    },
    error: function(data) {
      console.error(data.responseJSON.errors);
    },
  });
}

function deletetemplate(id) {
  event.stopPropagation();
  var tem = $(event.target);
  if (!confirm("related pages will be deleted !\n are you sure?")) return;
  $.ajax({
    url: "/admin/templates/delete",
    type: "POST",
    data: { id: id },
    success: function(data) {
      console.log(data);
      tem.closest(".templatecard").remove();
    },
    error: function(data) {
      console.log(data);
    },
  });
}

function deletepage(id) {
  event.preventDefault();
  event.stopPropagation();
  var tem = $(event.target);
  if (!confirm("page contents will be deleted !\n are you sure?")) return;
  $.ajax({
    url: "/admin/pages/delete",
    type: "POST",
    data: { id: id },
    success: function(data) {
      console.log(data);
      tem.closest(".pageitm").remove();
    },
    error: function(data) {
      console.log(data);
    },
  });
}
