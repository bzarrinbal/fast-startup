$(function() {
  $("[data-bz-id]")
    .addClass("adminedit")
    .attr({ contenteditable: "" });
  $("body").prepend(
    '<div class="admin_register admin_btn" ><span class="dragable"></span><div class="btn"  onclick="adminregisterpagecontent()">ثبت تغییرات</div></div><div class="admin_cancel admin_btn" ><span class="dragable"></span><div class="btn" onclick="location.reload(true);">لغو تغییرات</div></div><div class="admin_logout admin_btn" ><span class="dragable"></span><div class="btn"  onclick="logout()">خروج</div></div>'
  );
  dragElement($(".admin_btn")[0]);
  dragElement($(".admin_btn")[1]);
  dragElement($(".admin_btn")[2]);
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
  });
});

// draggable buttons
function dragElement(elmnt) {
  // e = e || window.event;
  // e.preventDefault();
  // e.stopPropagation();
  var pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  elmnt.getElementsByClassName("dragable")[0].onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = elmnt.offsetTop - pos2 + "px";
    elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
  }

  function closeDragElement(e) {
    e = e || window.event;
    e.preventDefault();
    e.stopPropagation();
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

// register changes
function adminregisterpagecontent() {
  var data = {
    pageurl: window.location.pathname.slice(1),
    pagecontent: {},
    template_id: window.template_id,
  };
  $("[data-bz-id]").each(function(i, el) {
    data.pagecontent["data_id_" + $(el).data("bz-id")] = $(el)
      .clone() //clone the element
      .children() //select all the children
      .remove() //remove all the children
      .end() //again go back to selected element
      .text();
  });
  $("[data-bz-href-id]").each(function(i, el) {
    data.pagecontent["data_id_" + $(el).data("bz-href-id")] = $(el).attr(
      "href"
    );
  });

  $.ajax({
    url: "/admin/contentedit",
    type: "POST",
    data: data,
    success: function(data) {
      console.log(data);
      alert("تغییرات با موفقیت ثبت گردید");
    },
    error: function(data) {
      alert("خطا ...");
    },
  });
}

//logout
function logout() {
  $.ajax({
    url: "/logout",
    type: "POST",
    success: function(data) {
      // console.log(data);
      location.reload(true);
    },
    error: function(data) {
      alert("خطا ...");
    },
  });
}
