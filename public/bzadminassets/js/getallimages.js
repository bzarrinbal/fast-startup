function getallBgimages() {
  var url,
    id = 1,
    B = [],
    A = document.getElementsByTagName("*");
  A = B.slice.call(A, 0, A.length);
  while (A.length) {
    a = A.shift();
    url = document.deepCss(a, "background-image");
    if (url) url = /url\(['"]?([^")]+)/.exec(url) || [];
    url = url[1];
    if (url) {
      a.setAttribute("data-bz-img-type", "bg");
      if (B.indexOf(url) == -1) {
        B[B.length] = url;
        a.setAttribute("data-bz-img-id", B.length);
      } else {
        a.setAttribute("data-bz-img-id", B.indexOf(url));
      }
    }
  }
  return B;
}

document.deepCss = function(who, css) {
  if (!who || !who.style) return "";
  var sty = css.replace(/\-([a-z])/g, function(a, b) {
    return b.toUpperCase();
  });
  if (who.currentStyle) {
    return who.style[sty] || who.currentStyle[sty] || "";
  }
  var dv = document.defaultView || window;
  return (
    who.style[sty] || dv.getComputedStyle(who, "").getPropertyValue(css) || ""
  );
};

Array.prototype.indexOf =
  Array.prototype.indexOf ||
  function(what, index) {
    index = index || 0;
    var L = this.length;
    while (index < L) {
      if (this[index] === what) return index;
      ++index;
    }
    return -1;
  };





//jquery needed
var images = [];
$("*").each(function(i, e) {
  var target = $(e);
  if (target.prop("tagName") == "IMG") {
    images.push(target.prop("src"));
  }
  background = window
    .getComputedStyle(e)
    .getPropertyValue("background-image", "")
    .slice(5, -2);
  beforebackground = window
    .getComputedStyle(e, ":before")
    .getPropertyValue("background-image")
    .slice(5, -2);
  afterbackground = window
    .getComputedStyle(e, ":after")
    .getPropertyValue("background-image")
    .slice(5, -2);
  if (
    beforebackground.length > 3 &&
    beforebackground.slice(0, 4) == "http" &&
    images.indexOf(beforebackground) < 0
  )
    images.push(beforebackground);
  if (
    background.length > 3 &&
    background.slice(0, 4) == "http" &&
    images.indexOf(background) < 0
  )
    images.push(background);
  if (
    afterbackground.length > 3 &&
    afterbackground.slice(0, 4) == "http" &&
    images.indexOf(afterbackground) < 0
  )
    images.push(afterbackground);
});
console.log(images);




var images = [];
elements = document.querySelectorAll("*");
 images.forEach.call(elements,function(e){

  if (e.tagName == "IMG") {
    images.push(e.src);
  }
  background = window
    .getComputedStyle(e)
    .getPropertyValue("background-image", "")
    .slice(5, -2);
  beforebackground = window
    .getComputedStyle(e, ":before")
    .getPropertyValue("background-image")
    .slice(5, -2);
  afterbackground = window
    .getComputedStyle(e, ":after")
    .getPropertyValue("background-image")
    .slice(5, -2);
  if (
    beforebackground.length > 3 &&
    beforebackground.slice(0, 4) == "http" &&
    images.indexOf(beforebackground) < 0
  )
    images.push(beforebackground);
  if (
    background.length > 3 &&
    background.slice(0, 4) == "http" &&
    images.indexOf(background) < 0
  )
    images.push(background);
  if (
    afterbackground.length > 3 &&
    afterbackground.slice(0, 4) == "http" &&
    images.indexOf(afterbackground) < 0
  )
    images.push(afterbackground);
});
console.log(images);
