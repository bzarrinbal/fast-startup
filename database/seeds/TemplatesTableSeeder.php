<?php

use Illuminate\Database\Seeder;

class TemplatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('templates')->delete();

        \DB::table('templates')->insert(array (
            0 =>
            array (
                'id' => '1',
                'title' => 'صفحه اصلی ویزیت',
                'description' => 'طراحی توسط علیرضا عباسی',
                'image' => NULL,
                'template_file' => NULL,
                'html_file' => NULL,
                'downloaded_url' => NULL,
                'view' => 'index',
                'default_content' => '{test: adsf}',
                'created_at' => NULL,
            )
        ));


    }
}
