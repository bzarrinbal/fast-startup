<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

      // phonenumber 09125086039
      //password visit21158jalaian
        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'name' => 'کاربر ادمین',
                'email' => 'babak.zarrinbal@gmail.com',
                'phonenumber' => '9876543210',
                'password' => bcrypt('0123456789'),
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));


    }
}
