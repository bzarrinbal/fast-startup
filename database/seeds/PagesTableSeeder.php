<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('pages')->delete();

        \DB::table('pages')->insert(array (
            0 =>
            array (
                'id' => '1',
                'title' => 'صفحه اصلی',
                'url' => NULL,
                'image' => NULL,
                'description' => 'صفحه معرفی ویزیت',
                'updated_at' => NULL,
            )
        ));
      }

    }
